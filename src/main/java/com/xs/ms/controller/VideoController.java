package com.xs.ms.controller;

import com.xs.ms.utils.FileListUtils;
import com.xs.ms.utils.SysFiles;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.slf4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.ContextLoader;
import org.springframework.web.context.WebApplicationContext;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.URLDecoder;
import java.util.Date;

/**
 * Created by xvshu on 2019/7/6.
 */
@Controller
public class VideoController {

    private static Log log = LogFactory.getLog(VideoController.class);

    /**
     * 大文件分块下载
     *
     * @param request
     * @param response
     * @throws IOException
     */
    @RequestMapping("/sys/video/**")
    public void vedioPlay(String file_uri,HttpServletRequest request,
                                HttpServletResponse response) throws IOException {
        //String file_uri = request.getParameter("file_uri");

        log.info("=vedioPlay=>file_uri:"+file_uri);
        // 要下载的文件
        File file = new File(file_uri);
        if(file.exists()) {
            FileInputStream in = new FileInputStream(file);
            OutputStream os = response.getOutputStream();
            byte[] b = new byte[1024];
            while(in.read(b)!= -1) {
                os.write(b);
            }
            in.close();
            os.flush();
            os.close();
        }
    }

    protected void copyRange(InputStream istream, OutputStream ostream,
                             long start, long end) throws IOException {

        long skipped = 0;
        skipped = istream.skip(start);

        if (skipped < start) {
            throw new IOException("skip fail: skipped=" + Long.valueOf(skipped)
                    + ", start=" + Long.valueOf(start));
        }

        long bytesToRead = end - start + 1;

        byte buffer[] = new byte[2048];
        int len = buffer.length;
        while ((bytesToRead > 0) && (len >= buffer.length)) {
            try {
                len = istream.read(buffer);
                if (bytesToRead >= len) {
                    ostream.write(buffer, 0, len);
                    bytesToRead -= len;
                } else {
                    ostream.write(buffer, 0, (int) bytesToRead);
                    bytesToRead = 0;
                }
            } catch (IOException e) {
                len = -1;
                throw e;
            }
            if (len < buffer.length) {
                break;
            }
        }

    }

    @RequestMapping(value = "/sys/next/flod")
    @ResponseBody
    public SysFiles getImg(String file_uri,HttpServletResponse response){
        log.info("file_uri:"+file_uri);
        try {
            if (file_uri == null || file_uri.trim().length() == 0) {
                file_uri = FileListUtils.initFile;
            }
            SysFiles result = FileListUtils.getNextList(file_uri,"mp4");
            return result;
        }catch (Exception ex){
            ex.printStackTrace();
        }
        return null;
    }
}
