package com.xs.ms.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;

/**
 * Created by xvshu on 2019/7/5.
 */
@Controller
public class RoulContoller {

    @RequestMapping(value = "/img")
    public String imgIndex(){
        return "img";
    }

    @RequestMapping(value = "/scroll")
    public String imgScroll(){
        return "scroll_img";
    }

    @RequestMapping(value = "/")
    public String index(){
        return "index";
    }

    @RequestMapping(value = "/vedio")
    public String vodio(){
        return "vodio";
    }
}
