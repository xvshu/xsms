package com.xs.ms.controller;

import com.alibaba.fastjson.JSON;
import com.xs.ms.utils.FileListUtils;
import com.xs.ms.utils.SysFiles;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.List;
import java.util.Vector;

/**
 * Created by xvshu on 2019/7/5.
 */
@Controller
@RequestMapping(value = "/sys/img")
public class FileImgContorller {

    private static Vector<String> imgs = new Vector<String>();

    static{
        List<String> imgTypes= Arrays.asList("jpg.png.gif.jpeg.JPG.PNG.GIF.JPEG".split("\\."));
        try {
            SysFiles result = FileListUtils.getAllList(FileListUtils.initFile);
            System.out.println(JSON.toJSONString(result));
            if(result!=null && result.getFiles()!=null && result.getFiles().size()>0){
                for(String oneF : result.getFiles()){
                    for(String oneT : imgTypes){
                        if(oneF.endsWith(oneT)){
                            imgs.add(oneF);
                            break;
                        }
                    }
                }
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }

    @RequestMapping(value = "/get")
    public void getImg(Integer index,HttpServletResponse response){
        try{
            if(index==null){
                index=0;
            }
            String imgUrl = null;
            if(imgs!=null&&imgs.size()>0){
                if(index>=0){
                    imgUrl=imgs.get(index);
                }else{
                    imgUrl=imgs.get(0);
                }
            }else{
                return;
            }
            File file = new File(imgUrl);
            if(file.exists()) {
                FileInputStream in = new FileInputStream(file);
                OutputStream os = response.getOutputStream();
                byte[] b = new byte[1024];
                while(in.read(b)!= -1) {
                    os.write(b);
                }
                in.close();
                os.flush();
                os.close();
            }
        }catch (Exception ex){
          ex.printStackTrace();
        }

    }
    @RequestMapping(value = "/getmax")
    @ResponseBody
    public int getAllImgs(){
        return  imgs.size();
    }
}
