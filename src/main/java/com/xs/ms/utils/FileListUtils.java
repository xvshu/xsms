package com.xs.ms.utils;

import java.io.*;
import java.util.Vector;

/**
 * Created by xvshu on 2019/7/5.
 */
public class FileListUtils {

    public static final String initFile="/Users/xvshu/Documents";
    public static SysFiles getNextList(String dir_name) throws Exception{
        SysFiles sf = new SysFiles();
        Vector<String> ver=new Vector<String>();  //存储路径
        Vector<String> fils = new Vector<String>(); //存储文件


        File[] files = new File(dir_name).listFiles();  //获取该文件夹下所有的文件(夹)名
        int len=files.length;

        for(int i=0;i<len;i++){

            String tmp=files[i].getAbsolutePath();

            if(files[i].isDirectory()) {  //如果是目录，则加入队列。以便进行后续处理
                ver.add(tmp);
            }else {
                fils.add(tmp);
                //如果是文件，则直接输出文件名到指定的文件。
            }
        }

        sf.setFiles(fils);
        sf.setVers(ver);
        return sf;
    }

    public static SysFiles getNextList(String dir_name,String fileType) throws Exception{
        SysFiles sf = new SysFiles();
        Vector<String> ver=new Vector<String>();  //存储路径
        Vector<String> fils = new Vector<String>(); //存储文件


        File[] files = new File(dir_name).listFiles();  //获取该文件夹下所有的文件(夹)名
        int len=files.length;

        for(int i=0;i<len;i++){

            String tmp=files[i].getAbsolutePath();

            if(files[i].isDirectory()) {  //如果是目录，则加入队列。以便进行后续处理
                ver.add(tmp);
            }else {
                if(tmp.trim().endsWith(fileType)){
                    fils.add(tmp);
                }
                //如果是文件，则直接输出文件名到指定的文件。
            }
        }

        sf.setFiles(fils);
        sf.setVers(ver);
        return sf;
    }

    public static SysFiles getAllList(String dir_name) throws Exception{
        SysFiles sf = new SysFiles();
        Vector<String> ver=new Vector<String>();  //存储路径
        Vector<String> fils = new Vector<String>(); //存储文件
        ver.add(dir_name);
        while(ver.size()>0){

            File[] files = new File(ver.get(0).toString()).listFiles();  //获取该文件夹下所有的文件(夹)名
            ver.remove(0);
            int len=files.length;

            for(int i=0;i<len;i++){

                String tmp=files[i].getAbsolutePath();

                if(files[i].isDirectory()) {  //如果是目录，则加入队列。以便进行后续处理
                    ver.add(tmp);
                }else {
                    fils.add(tmp);
                    //如果是文件，则直接输出文件名到指定的文件。
                }
            }

        }

        sf.setFiles(fils);
        sf.setVers(ver);
        return sf;
    }
}
