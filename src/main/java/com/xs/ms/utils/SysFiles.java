package com.xs.ms.utils;

import java.util.Vector;

/**
 * Created by xvshu on 2019/7/5.
 */
public class SysFiles {
    Vector<String> vers;
    Vector<String> files;

    public Vector<String> getVers() {
        return vers;
    }

    public void setVers(Vector<String> vers) {
        this.vers = vers;
    }

    public Vector<String> getFiles() {
        return files;
    }

    public void setFiles(Vector<String> files) {
        this.files = files;
    }
}
