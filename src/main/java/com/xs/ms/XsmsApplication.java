package com.xs.ms;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class XsmsApplication {

	public static void main(String[] args) {
		SpringApplication.run(XsmsApplication.class, args);
	}

}
